
// Mini - Activity

console.log("");
const savings = 8 ** 3;
console.log(`The cube of 8 is: ${savings}`);


// Mini - Activity 2

const address = ["258", "Washington Ave NW", "California", "90011"];
const [block,ave,state,zip] = address;

console.log(`I live at ${block} ${ave}, ${state}, ${zip}`);

// Mini - Activity

const animal =
{
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075kgs",
	measurement: "20ft 3 in"
}

const { name, species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);


// Mini- Activity

let numbers = [1,2,3,4,5];
let result;

numbers.forEach((number) =>
{
	console.log(number);

});
result = numbers.reduce((a,b) =>
	{
		return a + b;
	});

console.log(result);


// Mini - Activity

class Dog
{
	constructor(name,age,breed)
	{
		this.name = name;
		this.age = age;
		this.breed = breed;
	}	
}
let newDog = new Dog("Ed-ward",12,"Chimera?");
console.log(newDog);