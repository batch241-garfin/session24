
// ES6 Updates

// Exponent Operator
	
// Old

console.log("Result of Old JS:");
const oldNum = Math.pow(8,2);
console.log(oldNum);

// New

console.log("Result of New JS:");
const newNum = 8 ** 2;
console.log(newNum);


// Template Literals

	// Allows us to write without using the concatenate operator (+)

let studentName = "Roland";

// Pre-template Literal String

console.log("Pre-template literal:");
console.log("Hello "+ studentName + "! Welcome to programming.");

// Template Literals String

console.log("Template literal:");
console.log(`Hello ${studentName}! Welcome to programming.`);

// Muli-line Template Literal

const message =
`${studentName} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of
${newNum}.
`
console.log(message);

// Template literals allow us to write strings with embedded Javascript expressions.
// ${} are used to include Javascript expressions in string using template literals

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings account is: ${principal * interestRate}`);

// Array Desctructuring

// Allows us to unpack elements in arrays into distinct variables
// Allows us to name array elements with variables instead of using index numbers

// Syntax:
// let/const [variableName, variableName, variableName] = array;

console.log("Array Desctructuring:");
const fullName =["Jeru","Nebur","Palma"];

// Pre-array Desctructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Array Desctructuring

console.log("Array Desctructuring:");
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

// Object Desctructuring

// Allows us to unpack properties on objects into distinct values.
// Shortens the syntax for accessing properties from objects

// Syntax:
// let/const { propertyName, propertyname, propertyname } = object;

const person =
{
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-object Desctructuring

console.log("Pre-Object Desctructuring:");
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`);

// Object Desctructuring

const { givenName, maidenName, familyName} = person;
console.log("Object Desctructuring:");
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}`);


// Arrow Functions

// Compact alternative syntax to traditional functions
// Useful for creating code snippets where creating functions will not be reused in any other portions of the code
// Adheres to the DRY principle (Dont Repeat Yourself) principle where there is no longer a need to create a new function and think of a name for functions that will only be used in certain code snippets.

// Syntax:
// let/const variableName = (parameterA, parameterB,parameterC) => {};

const hello = () =>
{
	console.log("Good Morning Batch 241!");
};


const printFullName = (firstN, middleI, lastN) =>
{
	console.log(`${firstN} ${middleI} ${lastN}`);
} 

printFullName("John", "D.", "Smith");
printFullName("Eric", "A.", "Andales");

const students = ["John","Jane","Smith"];

// Arrow function with loops

// Old

students.forEach(function(student)
{
	console.log(`${student} is a student.`);
})

// New
students.forEach((student) =>
{
	console.log(`${student} is a student`);
})

// Implicit Return Statement

// There are instances where you can omit the return statement
// This works because even without the return statement, Javascript add for it for the result of the function.

const add = (x,y) =>
{
	return x+y;
}

let total = add(1,2);
console.log(total);

const subtract = (x,y) => x - y;
let difference = subtract(3,1);
console.log(difference);


// Default Argument Value

// PRovides a default argument if none is provided when the function is invoked.

const greet =(name = "User") =>
{
	return `Good Morning, ${name}`
}
console.log(greet());
console.log(greet("Jeru"));


// Class-based Object Blueprints

// Allows creation/instantiation of objects as blueprints

// Creating a class
// The constructor function is a special method of class for creating/initializing an object for that class.
//The this keyword refers to the properties initialized from the inside the class.

// Syntax:
// class className
// {
	// constructor(objectPropertyA, objectPropertyB)
	// {
		//this.objectPropertyA = objectPropertyA;
		//this.objectPropertyB = objectPropertyB;
	// }
// }

class Car
{
	constructor(brand, name, year)
	{
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
} 

// The new operator creates/instantiates a new object with the given arguments as the value

// Syntax:
// let/const vriableName = new className();

console.log("Class Blueprints");
let myCar = new Car()
console.log(myCar); // undefined

// Assigning properties after creation/instantiation of an object

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// Instantiate a new object

const myNewCar = new Car("Toyota","Vios","2021");
console.log(myNewCar);

